package com.daksa.crm.ping.engine;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Nafhul A
 */
public class Mail {

    private static final Logger logger = LoggerFactory.getLogger(Mail.class);
    
    static Properties mailServerProperties;
    static Session getMailSession;
    static MimeMessage generateMailMessage;
    static String mailTo;
    
    public static void generateAndSendEmail(String emailBody) throws AddressException, MessagingException {

        // Step1
        logger.info("\n 1st ===> setup Mail Server Properties..");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        System.out.println("Mail Server Properties have been setup successfully..");

        // Step2
        logger.info("\n\n 2nd ===> get Mail Session..");
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        generateMailMessage = new MimeMessage(getMailSession);
        String[] mailToWhom = mailTo.split(";");
        logger.info("List of recipient :");
        for (String recipient : mailToWhom) { 
            logger.info(recipient);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        }        
        //generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("bastiyan.parmadi@daksa.co.id"));
        //generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("test2@crunchify.com"));
        generateMailMessage.setSubject("CRM STATUS");        
        generateMailMessage.setContent(emailBody, "text/html");
        logger.info("Mail Session has been created successfully..");

        // Step3
        logger.info("\n\n 3rd ===> Get Session and Send mail");
        Transport transport = getMailSession.getTransport("smtp");

        // Enter your correct gmail UserID and Password        
        transport.connect("smtp.gmail.com", "noreply@daksa.co.id", "d4ksanoreply");
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }
}
