package com.daksa.crm.ping.engine;

import com.daksa.crm.ping.main.MainMonitoring;
import com.daksa.crm.ping.model.ConfigModel;
import com.daksa.crm.ping.model.PingModel;
import com.daksa.rest.client.AuthType;
import com.daksa.rest.client.RestRequestBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.security.SignatureException;
import java.util.Date;
import javax.mail.MessagingException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Nafhul A
 */
public class Ping {

    private static final Logger logger = LoggerFactory.getLogger(Ping.class);

    public Ping() {
        try {
            ConfigModel configModel = readJsonFromFile();
            Mail.mailTo = configModel.getEmailTo();
            RestRequestBuilder requestBuilder = new RestRequestBuilder();
            requestBuilder.setAuthType(AuthType.CUSTOM);
            requestBuilder.setDate(new Date());
            requestBuilder.setRequestUrl(configModel.getAddress());
            requestBuilder.setContentType("application/json");
            requestBuilder.setMethod("GET");
            requestBuilder.send();
            byte[] responseBody = requestBuilder.getResponseBody();
            String response = new String(responseBody);
            logger.info("Response Code :" + requestBuilder.getResponseCode());
            if (requestBuilder.getResponseCode() == 200) {
                PingModel model = new ObjectMapper().readValue(response, PingModel.class);
                logger.info("databaseNote : " + model.getDatabaseNotes());
                logger.info("qvaNotes : " + model.getQvaNotes());
                logger.info("qvaOk : " + model.getQvaOk());
                logger.info("databaseOk : " + model.getDatabaseOk());
                if (model.getDatabaseNotes() != null) {
                    String emailBody = "CRM WEB tidak dapat diakses dikarenakan koneksi ke DATABASE gagal : " + model.getDatabaseNotes();
                    Mail.generateAndSendEmail(emailBody);
                }
                if (model.getQvaNotes() != null) {
                    String emailBody = "CRM WEB tidak dapat diakses dikarenakan koneksi ke QVA gagal : " + model.getQvaNotes();
                    Mail.generateAndSendEmail(emailBody);
                }
            } else {
                logger.info(response);
                logger.info(requestBuilder.getResponseMessage());
                Mail.generateAndSendEmail(response);
            }
        } catch (IOException | SignatureException | MessagingException | ParseException | URISyntaxException e) {
            logger.error(e.getMessage());
            try {
                Mail.generateAndSendEmail(e.getMessage());
                System.out.println("\n\n ===> Email sent successfully..");
            } catch (MessagingException ex) {
                logger.error(ex.getMessage());
            }
        }
    }

    private ConfigModel readJsonFromFile() throws IOException, ParseException, URISyntaxException {
        JSONParser parser = new JSONParser();
        CodeSource codeSource = MainMonitoring.class.getProtectionDomain().getCodeSource();
        File jarFile = new File(codeSource.getLocation().toURI().getPath());
        String jarDir = jarFile.getParentFile().getPath();
        String configFile = jarDir + "/config.txt";        
        logger.info(jarDir); 
        Object obj = parser.parse(new FileReader(configFile));
        JSONObject jsonObject = (JSONObject) obj;
        String address = (String) jsonObject.get("address");
        String emailTo = (String) jsonObject.get("emailTo");
        ConfigModel configModel = new ConfigModel(address, emailTo);
        logger.info("Address: " + configModel.getAddress());
        logger.info("Email To: " + configModel.getEmailTo());
        return configModel;
    }
}
