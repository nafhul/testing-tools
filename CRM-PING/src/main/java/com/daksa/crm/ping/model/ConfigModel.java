/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.daksa.crm.ping.model;

/**
 *
 * @author Nafhul A
 */
public class ConfigModel {
    
    private final String address;
    private final String emailTo;

    public ConfigModel(String address, String emailTo) {
        this.address = address;
        this.emailTo = emailTo;
    }

    public String getAddress() {
        return address;
    }

    public String getEmailTo() {
        return emailTo;
    }

}
