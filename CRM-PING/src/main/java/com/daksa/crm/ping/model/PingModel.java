package com.daksa.crm.ping.model;

/**
 *
 * @author Nafhul A
 */
public class PingModel {
    private String databaseNotes;
    private String qvaNotes;
    private String qvaOk;
    private String databaseOk;

    public PingModel() {
    }

    public String getDatabaseNotes() {
        return databaseNotes;
    }

    public void setDatabaseNotes(String databaseNotes) {
        this.databaseNotes = databaseNotes;
    }

    public String getQvaNotes() {
        return qvaNotes;
    }

    public void setQvaNotes(String qvaNotes) {
        this.qvaNotes = qvaNotes;
    }

    public String getQvaOk() {
        return qvaOk;
    }

    public void setQvaOk(String qvaOk) {
        this.qvaOk = qvaOk;
    }

    public String getDatabaseOk() {
        return databaseOk;
    }

    public void setDatabaseOk(String databaseOk) {
        this.databaseOk = databaseOk;
    }
    
    
}
